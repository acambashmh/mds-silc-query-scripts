﻿var request = require('request');
var q = require('q');
var querystring = require('querystring');
var fs = require('fs');


function createMdsUrl(hostUrl, program, grade) {
    //var result = hostUrl + '/api/search/metadata/' + '?q=(program:"' + program + '" grade:' + grade + ' resources_panel_te:true)' + '&collection=Resources&options=search-ebook-json&format=json';
    var result = hostUrl + '/api/search/v1/toc/' + program + '/' + grade + '?outputFormat=json&view=player&showResources=true&showGradeResources=true';
    return result;
}

function querySilcForGradeLvlResources(hostUrl, username, password, program, grade, persist) {
    
    //create url
    var hostUrl = 'http://my-test.hrw.com' || hostUrl;
    
    q.fcall(function () {
        var defer = q.defer();
        var user = {
            username: username || "Ent9477@6y" ,
            password: password || "Ent9477@6y",
            grant_type: 'password'
        };
        
        request.post(hostUrl + '/api/identity/v1/token;contextId=hmof', 
        {
            headers: { "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8" },
            body: [querystring.stringify(user)]
        },
         function (err, req, res) {
            defer.resolve(JSON.parse(res));
        });
        return defer.promise;
    }).then(function (data) {
        var defer = q.defer();
        
        //var url = 'http://my-test.hrw.com/api/search/v1/toc/HMH Collections/08?outputFormat=json&view=player&showResources=true&showGradeResources=true';
        var url = createMdsUrl(hostUrl, program, grade)
        request.get(url, {
            headers: {
                'Authorization': data.access_token
            }
        }, function (err, req, res) {
            
            if (err) {
                console.log(err);
            }
            try {
                var result = JSON.parse(res);
                
                defer.resolve(result);
            } catch (e) {
                defer.reject(e);
            }

        });
        
        return defer.promise;

    }).then(function (data) {
        
        var defer = q.defer();

        if (persist) {
            try {
                fs.writeFileSync('result.json', JSON.stringify(data));
                defer.resolve(data);
            } catch (e) {
                console.log(e.toString());
                defer.reject(e);
            }
        }

        return defer.promise;
    })
}

module.exports = querySilcForGradeLvlResources;

//querySilcForGradeLvlResources('http://my-test.hrw.com', "Ent9477@6y", "Ent9477@6y", 'HMH Collections', '08');